#!/bin/bash
set -e
echo "Fetch updates..."
git pull
echo "Publish changes..."
cp ./src/latex/main.pdf ./src/website/assets/cv-eduardo-subtil.pdf
echo "Done."
set +e